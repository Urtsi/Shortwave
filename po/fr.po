# French translation for shortwave.
# Copyright (C) 2020-2021 shortwave's COPYRIGHT HOLDER
# This file is distributed under the same license as the shortwave package.
# Thibault Martin <mail@thibaultmart.in>, 2020.
# Charles Monzat <charles.monzat@free.fr>, 2022.
# Claude Paroz <claude@2xlibre.net>, 2021-2022.
#
msgid ""
msgstr ""
"Project-Id-Version: shortwave master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/Shortwave/issues\n"
"POT-Creation-Date: 2024-10-12 15:06+0000\n"
"PO-Revision-Date: 2024-10-16 13:00+0200\n"
"Last-Translator: Irénée Thirion <irenee.thirion@e.email>\n"
"Language-Team: GNOME French Team <gnomefr@traduc.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 3.5\n"

#: data/de.haeckerfelix.Shortwave.desktop.in.in:3
#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:5
msgid "@NAME@"
msgstr "@NAME@"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/de.haeckerfelix.Shortwave.desktop.in.in:12
msgid "Gradio;Radio;Stream;Wave;"
msgstr "Gradio;radio;stream;flux;wave;diffusion;"

#. General
#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:10
msgid "Listen to internet radio"
msgstr "Écouter une radio internet"

#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:12
msgid ""
"Shortwave is an internet radio player that provides access to a station "
"database with over 50,000 stations."
msgstr ""
"Shortwave est un lecteur de radios internet qui permet d’accéder à une base "
"de données de plus de 50 000 stations."

#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:15
msgid "Features:"
msgstr "Fonctionnalités :"

#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:17
#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:64
msgid "Create your own library where you can add your favorite stations"
msgstr "Créez votre propre bibliothèque où ajouter vos stations préférées"

#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:18
#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:68
msgid "Easily search and discover new radio stations"
msgstr "Cherchez et découvrez de nouvelles stations de radio"

#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:19
msgid ""
"Automatic recognition of songs, with the possibility to save them "
"individually"
msgstr ""
"Reconnaissance automatique des chansons, avec possibilité de les enregistrer "
"individuellement"

#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:20
#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:72
msgid "Responsive application layout, compatible for small and large screens"
msgstr "Interface adaptative, compatible avec les petits et grands écrans"

#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:21
msgid "Play audio on supported network devices (e.g. Google Chromecasts)"
msgstr ""
"Diffusion audio sur les appareils réseau pris en charge (par exemple les "
"Google Chromecasts)"

#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:22
msgid "Seamless integration into the GNOME desktop environment"
msgstr "Intégration transparente à l’environnement de bureau GNOME"

#. Branding
#: data/de.haeckerfelix.Shortwave.metainfo.xml.in.in:28
msgid "Felix Häcker"
msgstr "Felix Häcker"

#: data/gtk/add_station_dialog.ui:14
msgid "Add Local Station"
msgstr "Ajouter une station locale"

#: data/gtk/add_station_dialog.ui:64
msgid "Change Station Cover"
msgstr "Modifier l’image de la station"

#: data/gtk/add_station_dialog.ui:95
msgid ""
"This station will only be added to your library. If you want it to be "
"available to everyone, you should add it to the <a href=\"https://www.radio-"
"browser.info/add\">public database</a>."
msgstr ""
"Cette station ne sera ajoutée qu’à votre bibliothèque. Pour la rendre "
"disponible à tous, ajoutez-la à la <a href=\"https://www.radio-browser.info/"
"add\">base de données publique</a>."

#: data/gtk/add_station_dialog.ui:107
msgid "Name"
msgstr "Nom"

#: data/gtk/add_station_dialog.ui:115
msgid "Stream URL"
msgstr "URL du flux"

#: data/gtk/add_station_dialog.ui:123
msgid "Add Station"
msgstr "Ajouter une station"

#: data/gtk/device_dialog.ui:7 data/gtk/player_view.ui:124
msgid "Connect Device"
msgstr "Connecter un appareil"

#: data/gtk/device_dialog.ui:18
msgid "Searching for Devices…"
msgstr "Recherche d’appareils…"

#: data/gtk/device_dialog.ui:25
msgid "Search for Devices"
msgstr "Rechercher des appareils"

#: data/gtk/device_dialog.ui:41
msgid "No Devices Available"
msgstr "Aucun appareil disponible"

#: data/gtk/device_dialog.ui:42
msgid "No supported Google Cast device found"
msgstr "Aucun service Google Cast pris en charge trouvé"

#: data/gtk/device_indicator.ui:7
msgid "Disconnect From Device"
msgstr "Déconnecter de l’appareil"

#: data/gtk/help_overlay.ui:11 data/gtk/settings_dialog.ui:9
msgid "General"
msgstr "Général"

#: data/gtk/help_overlay.ui:14
msgctxt "shortcut window"
msgid "Open shortcut window"
msgstr "Ouvrir la fenêtre des raccourcis"

#: data/gtk/help_overlay.ui:21
msgctxt "shortcut window"
msgid "Open application menu"
msgstr "Ouvrir le menu application"

#: data/gtk/help_overlay.ui:26
msgctxt "shortcut window"
msgid "Open preferences"
msgstr "Ouvrir les préférences"

#: data/gtk/help_overlay.ui:32
msgctxt "shortcut window"
msgid "Close the window"
msgstr "Fermer la fenêtre"

#: data/gtk/help_overlay.ui:38
msgctxt "shortcut window"
msgid "Quit the application"
msgstr "Quitter l’application"

#: data/gtk/help_overlay.ui:46
msgid "Playback"
msgstr "Lecture"

#: data/gtk/help_overlay.ui:49
msgctxt "shortcut window"
msgid "Toggle playback"
msgstr "Basculer la lecture"

#: data/gtk/library_page.ui:4
msgid "Shortwave"
msgstr "Shortwave"

#: data/gtk/library_page.ui:15 data/gtk/search_page.ui:4
msgid "Browse Stations"
msgstr "Parcourir les stations"

#: data/gtk/library_page.ui:23
msgid "Main Menu"
msgstr "Menu principal"

#: data/gtk/library_page.ui:92
msgid "_Add New Stations"
msgstr "_Ajouter de nouvelles stations"

#: data/gtk/library_page.ui:115
msgid "_Sorting"
msgstr "_Tri"

#: data/gtk/library_page.ui:118
msgid "_Name"
msgstr "_Nom"

#: data/gtk/library_page.ui:123
msgid "_Language"
msgstr "_Langue"

#: data/gtk/library_page.ui:128
msgid "_Country"
msgstr "_Pays"

#: data/gtk/library_page.ui:133
msgid "S_tate"
msgstr "É_tat"

#: data/gtk/library_page.ui:138
msgid "_Bitrate"
msgstr "Débit _binaire"

#: data/gtk/library_page.ui:145
msgid "_Ascending"
msgstr "C_roissant"

#: data/gtk/library_page.ui:150
msgid "_Descending"
msgstr "_Décroissant"

#: data/gtk/library_page.ui:159
msgid "_Open radio-browser.info <sup>↗</sup>"
msgstr "_Ouvrir radio-browser.info <sup>↗</sup>"

#: data/gtk/library_page.ui:166
msgid "_Preferences"
msgstr "_Préférences"

#: data/gtk/library_page.ui:170
msgid "_Keyboard Shortcuts"
msgstr "_Raccourcis clavier"

#: data/gtk/library_page.ui:174
msgid "_About Shortwave"
msgstr "À _propos de Shortwave"

#: data/gtk/library_page.ui:182
msgid "Add _Local Station"
msgstr "Ajouter une station _locale"

#: data/gtk/player_gadget.ui:40 data/gtk/player_toolbar.ui:92
#: data/gtk/player_view.ui:148 data/gtk/station_row.ui:86
msgid "Play"
msgstr "Lecture"

#: data/gtk/player_gadget.ui:55 data/gtk/player_toolbar.ui:106
#: data/gtk/player_view.ui:162
msgid "Stop"
msgstr "Arrêter"

#: data/gtk/player_gadget.ui:71 data/gtk/player_toolbar.ui:121
#: data/gtk/player_view.ui:177
msgid "Buffering…"
msgstr "Chargement…"

#: data/gtk/player_gadget.ui:118
msgid "SHORTWAVE INTERNET RADIO"
msgstr "RADIO INTERNET SHORTWAVE"

#: data/gtk/player_gadget.ui:140 data/gtk/player_toolbar.ui:29
#: data/gtk/player_view.ui:58
msgid "No Playback"
msgstr "Pas de lecture"

#: data/gtk/player_gadget.ui:199
msgid "Close"
msgstr "Fermer"

#: data/gtk/player_gadget.ui:210
msgid "Restore Window"
msgstr "Restaurer la fenêtre"

#: data/gtk/player_view.ui:13
msgid "Enable Gadget Mode"
msgstr "Activer le mode gadget"

#: data/gtk/player_view.ui:207
msgid "Show Station Details"
msgstr "Montrer les informations de la station"

#: data/gtk/player_view.ui:258
msgid "No Songs"
msgstr "Pas de chanson"

#: data/gtk/player_view.ui:269
msgid ""
"Songs that have been identified and recorded using the stream metadata will "
"appear here."
msgstr ""
"Les chansons identifiées et enregistrées en utilisant les métadonnées du "
"flux apparaîtront ici."

#: data/gtk/player_view.ui:303 data/gtk/recording_indicator.ui:38
msgid "An error occurred"
msgstr "Une erreur s’est produite"

#: data/gtk/search_page.ui:23
msgid "Search"
msgstr "Rechercher"

#: data/gtk/search_page.ui:24
msgid "Search stations"
msgstr "Rechercher des stations"

#: data/gtk/search_page.ui:72
msgid "Discover over 50,000 public stations"
msgstr "Découvrir plus de 50 000 stations"

#: data/gtk/search_page.ui:90
msgid "Popular Stations"
msgstr "Stations populaires"

#: data/gtk/search_page.ui:112
msgid "Discover New Stations"
msgstr "Découvrir de nouvelles stations"

#: data/gtk/search_page.ui:135
msgid ""
"The displayed information is provided by <a href=\"https://www.radio-browser."
"info/\">radio-browser.info</a>. \n"
"Shortwave has no influence on the listed stations."
msgstr ""
"Les informations affichées sont fournies par <a href=\"https://www.radio-"
"browser.info/\">radio-browser.info</a>.\n"
"Shortwave n’a aucune influence sur les stations listées."

#: data/gtk/search_page.ui:201
msgid "Loading…"
msgstr "Chargement…"

#: data/gtk/search_page.ui:216
msgid "No Results"
msgstr "Aucun résultat"

#: data/gtk/search_page.ui:217
msgid ""
"If the station you are looking for is missing, you can add it to the public "
"database."
msgstr ""
"Si la station que vous recherchez est manquante, vous pouvez l’ajouter à la "
"base de données publique."

#: data/gtk/search_page.ui:221
msgid "Add Missing Station"
msgstr "Ajouter une station manquante"

#: data/gtk/search_page.ui:239
msgid "Unable to Retrieve Station Data"
msgstr "Impossible d’analyser les données de la station"

#: data/gtk/search_page.ui:242
msgid "Try Again"
msgstr "Réessayer"

#: data/gtk/settings_dialog.ui:12
msgid "Features"
msgstr "Fonctionnalités"

#: data/gtk/settings_dialog.ui:15
msgid "_Notifications"
msgstr "_Notifications"

#: data/gtk/settings_dialog.ui:17
msgid "Show desktop notifications when a new song gets played"
msgstr "Afficher une notification système lorsqu’une nouvelle chanson commence"

#: data/gtk/song_row.ui:16
msgid "Save recorded song"
msgstr "Conserver une chanson enregistrée"

#: data/gtk/station_dialog.ui:73
msgid "_Play"
msgstr "_Lecture"

#: data/gtk/station_dialog.ui:89
msgid "_Add to Library"
msgstr "_Ajouter à la bibliothèque"

#: data/gtk/station_dialog.ui:106
msgid "_Remove From Library"
msgstr "_Enlever de la bibliothèque"

#: data/gtk/station_dialog.ui:125 data/gtk/station_row.ui:57
msgid "Local Station"
msgstr "Station locale"

#: data/gtk/station_dialog.ui:126
msgid ""
"This station exists only in your library, and is not part of the public "
"database"
msgstr ""
"Cette station existe uniquement dans votre bibliothèque et ne fait pas "
"partie de la base de données publique"

#: data/gtk/station_dialog.ui:140 data/gtk/station_row.ui:69
msgid "Orphaned Station"
msgstr "Station orpheline"

#: data/gtk/station_dialog.ui:141
msgid ""
"The information could not be updated, probably this station was removed from "
"the public database"
msgstr ""
"L’information n’a pas pu être mise à jour, il est probable que cette station "
"ait été enlevée de la base de données publique"

#: data/gtk/station_dialog.ui:152
msgid "Information"
msgstr "Informations"

#: data/gtk/station_dialog.ui:155
msgid "Language"
msgstr "Langue"

#: data/gtk/station_dialog.ui:165
msgid "Tags"
msgstr "Étiquettes"

#: data/gtk/station_dialog.ui:177
msgid "Location"
msgstr "Emplacement"

#: data/gtk/station_dialog.ui:181
msgid "Country"
msgstr "Pays"

#: data/gtk/station_dialog.ui:191
msgid "State"
msgstr "État"

#: data/gtk/station_dialog.ui:225
msgid "Audio"
msgstr "Audio"

#: data/gtk/station_dialog.ui:228
msgid "Bitrate"
msgstr "Débit binaire"

#: data/gtk/station_dialog.ui:238
msgid "Codec"
msgstr "Codec"

#. This is a noun/label for the station stream url
#: data/gtk/station_dialog.ui:248
msgid "Stream"
msgstr "Flux"

#: data/gtk/station_dialog.ui:257
msgid "Copy"
msgstr "Copier"

#: data/gtk/volume_control.ui:9
msgid "Toggle Mute"
msgstr "Basculer la sourdine"

#: data/gtk/volume_control.ui:21
msgid "Volume"
msgstr "Volume"

#: src/audio/player.rs:220
msgid "Active Playback"
msgstr "Lecture active"

#: src/audio/player.rs:382
msgid "Station cannot be streamed. URL is not valid."
msgstr "La station ne peut être diffusée. L’URL n’est pas correcte."

#: src/device/device_discovery.rs:83
msgid "Google Cast Device"
msgstr "Appareil Google Cast"

#: src/device/device_discovery.rs:86
msgid "Unknown Model"
msgstr "Modèle inconnu"

#: src/ui/about_dialog.rs:44
msgid "translator-credits"
msgstr ""
"Thibault Martin\n"
"Alexandre Franke\n"
"Claude Paroz\n"
"Charles Monzat\n"
"Louise Perrot\n"
"Irénée Thirion"

#: src/ui/add_station_dialog.rs:94
msgid "Select Station Cover"
msgstr "Choisir l’image de la station"

#: src/ui/display_error.rs:46
msgid "Show Details"
msgstr "Montrer les informations"

#: src/ui/pages/library_page.rs:111
msgid "Welcome to {}"
msgstr "Bienvenue dans {}"

#: src/ui/recording_indicator.rs:111
msgid "Recording in Progress"
msgstr "Enregistrement en cours"

#: src/ui/recording_indicator.rs:112
msgid "Ignored"
msgstr "Ignoré"

#: src/ui/recording_indicator.rs:113
msgid "No Recording"
msgstr "Aucun enregistrement"

#: src/ui/recording_indicator.rs:120
msgid "The current song will be recorded until a new song is detected."
msgstr ""
"La chanson actuelle sera enregistrée jusqu’à ce qu’une nouvelle chanson soit "
"détectée."

#: src/ui/recording_indicator.rs:123
msgid "No recording because the song title contains a word on the ignore list."
msgstr ""
"Aucun enregistrement car le titre de la chanson contient un mot dans la "
"liste à ignorer."

#: src/ui/recording_indicator.rs:126
msgid ""
"The current song cannot be fully recorded. The beginning has been missed."
msgstr ""
"La chanson actuelle n’a pas pu être enregistrée entièrement. Il manque le "
"début."

#: src/ui/station_dialog.rs:203
msgid "{} kbit/s"
msgstr "{} kbit/s"

